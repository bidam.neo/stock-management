<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{$title or "Login"}}</title>
    <link rel="icon" href="/images/icon/favicon.ico">
    <link href="{{asset('packages/bootstrap/css/bootstrap.min.css')}}"  rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <br><br><br>
            @yield('content')
        </div>
    </div>
</div>
<script src="{{asset('packages/jquery/js/jquery.min.js')}}"></script>
<script src="{{asset('packages/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>