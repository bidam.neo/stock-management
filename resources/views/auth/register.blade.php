@extends('layout.login')
@section("content")
        <div class="account-wall">
                <div id="my-tab-content" class="tab-content">
                        <div class="" id="register">
                                @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                        @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                        @endforeach
                                                </ul>
                                        </div>
                                @endif
                                <h3 class="text-center text-primary">Sign up</h3>
                                <form class="form-signin" action="/auth/register" method="post">
                                        <input type="text" name="name" class="form-control" placeholder="Name" required autofocus>
                                        <input type="email" name="email" class="form-control" placeholder="Emaill Address" required>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                                        <input type="submit" class="btn btn-lg btn-default btn-block" value="Sign Up" />
                                </form>
                                <div>
                                        <p class="text-center"><a href="/auth/login" >Have an Account?</a></p>
                                </div>
                        </div>
                </div>
        </div>

@endsection
