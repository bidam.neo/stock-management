@extends('layout.login')
@section("content")
        <div class="account-wall">
                <div id="my-tab-content" class="tab-content">
                        <div id="login">
                                @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                        @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                        @endforeach
                                                </ul>
                                        </div>
                                @endif
                                <img class="profile-img" src="{{asset('images/laravel.png')}}"
                                     alt="">
                                <form class="form-signin" action="/auth/login" method="post">
                                        <br>
                                        <input type="email" class="form-control" name="email" placeholder="Email" required autofocus >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <br>
                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                        <br>
                                        <input type="submit" class="btn btn-default btn-block" value="Sign In" />
                                </form>
                                <div>

                                </div>
                        </div>
                </div>
        </div>

@endsection
