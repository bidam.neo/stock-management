<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("number");
            $table->string("country");
            $table->string("code");
            $table->float("in_price");
            $table->float("out_price");
            $table->double("in_price_package");
            $table->double("out_price_package");
            $table->float("off_price");
            $table->date("make_date");
            $table->date("expire_date");

            $table->integer("product_type_id")->unsigned();
            $table->foreign('product_type_id')->references('id')->on('product_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
