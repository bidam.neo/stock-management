<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Supplier::class, 10)->create();
    }
}
