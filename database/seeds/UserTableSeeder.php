<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'          => 1,
                'email'       => 'bidam.neo@gmail.com',
                'password'    => '$2y$10$oOaU0I4Dea7zD0qZbBaKNuHCfa8/9XaFJxCBgpo/LHRS9b/AU5ZxS', // 123456
                'name'        => 'John Odom',
            ],
            [
                'id'          => 2,
                'email'       => 'admin@gmail.com',
                'password'    => '$2y$10$oOaU0I4Dea7zD0qZbBaKNuHCfa8/9XaFJxCBgpo/LHRS9b/AU5ZxS', // 123456
                'name'        => 'Administrator',
            ]
        ]);
    }
}
