<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_types')->insert([
            [
                'id'          => 1,
                'name'        => 'Type A',
            ],
            [
                'id'          => 2,
                'name'        => 'Type B',
            ],
            [
                'id'          => 3,
                'name'        => 'Type C',
            ],
            [
                'id'          => 4,
                'name'        => 'Type D',
            ],
            [
                'id'          => 5,
                'name'        => 'Type E',
            ]
        ]);
    }
}
