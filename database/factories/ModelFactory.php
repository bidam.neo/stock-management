<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'image' => $faker->image(),
        'email' => $faker->email,
        'company' => $faker->company,
        'add' => $faker->address,
        'tel' => $faker->phoneNumber,
        'position' => $faker->colorName,
        'status' => $faker->boolean()
    ];
});


$factory->define(App\Supplier::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'image' => $faker->image(),
        'company' => $faker->company,
        'email' => $faker->email,
        'add' => $faker->address,
        'tel' => $faker->phoneNumber,
        'status' => $faker->boolean()
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    $inprice = $faker->numberBetween(10,30);
    $outprice =$inprice + ($inprice*0.15);
    return [
        'name' => $faker->domainName,
        'number' => $faker->randomAscii,
        'country' => $faker->country,
        'code' => $faker->currencyCode,
        'in_price' => $inprice,
        'out_price' => $outprice,
        'in_price_package' => ($inprice*10) - ($inprice/2),
        'out_price_package' => ($outprice*10) - ($outprice/2),
        'off_price' => $inprice*0.2,
        'make_date' => $faker->dateTimeThisMonth,
        'expire_date' => $faker->dateTimeThisYear,
        'product_type_id' => $faker->numberBetween(1,5),
    ];
});