<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!!  Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300italic,300,500,700|Kantumruy:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="icon" href="/images/icon/favicon.ico">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
</head>
<body>

{!! Theme::partial('header') !!}

<div id="wrapper">
    <div id="page-wrapper">
        {!! Theme::content() !!}
        </div>
</div>

{!! Theme::partial('footer') !!}

{!! Theme::asset()->container('footer')->scripts() !!}
</body>
</html>