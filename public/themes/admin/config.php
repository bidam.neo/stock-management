<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Stock Mangment');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            $theme->asset()->add('bootstrap', 'packages/bootstrap/css/bootstrap.min.css');
            $theme->asset()->add('font-awesome', 'packages/font-awesome/css/font-awesome.min.css');
            $theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js');
            $theme->asset()->add('bootstrap', 'packages/bootstrap/js/bootstrap.min.js');
            $theme->asset()->add('select2', 'packages/select2/css/select2.min.css');
            $theme->asset()->add('datetimepicker', 'packages/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css');
            $theme->asset()->add('datetimepicker', 'packages/lightslider/css/lightslider.min.css');
            $theme->asset()->add('sb-admin-2', 'packages/sb-admin-2/css/sb-admin-2.css');
            $theme->asset()->usePath()->add('style', 'css/style.css');

            $theme->asset()->container('footer')->add('moment', 'packages/moment/js/moment-with-locales.min.js');
            $theme->asset()->container('footer')->add('datetimepicker', 'packages/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
            $theme->asset()->container('footer')->add('select2', 'packages/select2/js/select2.min.js');
            $theme->asset()->container('footer')->add('select2', 'packages/lightslider/js/lightslider.min.js');
//            $theme->asset()->container('footer')->add('sb-admin-2', 'packages/sb-admin-2/js/sb-admin-2.js');
            $theme->asset()->container('footer')->add('jquery confirm', 'packages/jQueryConfirm/jquery.confirm.js');
            $theme->asset()->container('footer')->usePath()->add('script', 'js/script.js');
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);