<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Supplier List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="/supplier/new" class="btn btn-primary " style="float:right; margin-bottom: 20px;"> <i class="fa fa-plus"></i> Add More</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Company</th>
                <th>Address</th>
                <th>email</th>
                <th>Telephone</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($suppliers as $sup)
                <tr>
                    <td>{{$sup->id}}</td>
                    <td>{{$sup->first_name}} {{$sup->last_name}}</td>
                    <td>{{$sup->company}}</td>
                    <td>{{$sup->add}}</td>
                    <td>{{$sup->email}}</td>
                    <td>{{$sup->tel}}</td>
                    <td width="10%">
                        <a href="/supplier/edit/{{$sup->id}}" class="btn btn-default"> <i class="fa fa-edit"></i> </a>
                        <a href="/supplier/delete/{{$sup->id}}" class="btn btn-danger delete"> <i class="fa fa-trash"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>