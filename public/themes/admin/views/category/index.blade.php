<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">Category List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-6">
        <a href="/category/new" class="btn btn-primary " style="float:right; margin-bottom: 20px;"> <i class="fa fa-plus"></i> Add More</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $cat)
                <tr>
                    <td>{{$cat->id}}</td>
                    <td>{{$cat->name}}</td>
                    <td width="25%">
                        <a href="/category/edit/{{$cat->id}}" class="btn btn-default"> <i class="fa fa-edit"></i> </a>
                        <a href="/category/delete/{{$cat->id}}" class="btn btn-danger delete"> <i class="fa fa-trash"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>