<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Category Edit</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-5">
            <form action="/category/add" class="form-horizontal" method="post">
                <label for="#page_title">Category Name</label>

                <input type="hidden" name="id" value="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="text" id="page_title" class="form-control" name="name" placeholder="Category Name" value=""/>
                <br>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Submit </button>
            </form>
    </div>
</div>