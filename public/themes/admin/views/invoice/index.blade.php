<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Invoice List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Number</th>
                <th>Customer</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $inv)
                <tr>
                    <td>{{$inv->id}}</td>
                    <td>{{$inv->first_name}} {{$cus->last_name}}</td>
                    <td>{{$inv->company}}</td>
                    <td>
                        <a href="#edit" class="btn btn-default"> <i class="fa fa-edit"></i></a>
                        <a href="#delete" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>