<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">New product</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-5">
            <form action="/product/add" class="form-horizontal" method="post">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label for="">Category</label>
                    <select class="form-control" name="category" id="">

                        @foreach($categories as $cat)
                            <option value="{{$cat->id}}" >{{$cat->name}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" id="" class="form-control" name="name" placeholder="Name" value=""/>
                </div>
                <div class="form-group">
                    <label for="">number</label>
                    <input type="text" id="" class="form-control" name="number" placeholder="number" value=""/>
                </div>
                <div class="form-group">
                    <label for="">country</label>
                    <input type="text" id="" class="form-control" name="country" placeholder="Company" value=""/>
                </div>
                <div class="form-group">
                    <label for="">code</label>
                    <input type="text" id="" class="form-control" name="code" placeholder="code" value=""/>
                </div>
                <div class="form-group">
                    <label for="">in_price</label>
                    <input type="text" required id="" class="form-control" name="in_price" placeholder="in_price" value=""/>
                </div>
                <div class="form-group">
                    <label for="">out_price</label>
                    <input type="text" id="" class="form-control" name="out_price" placeholder="out_price" value=""/>
                </div>
                <div class="form-group">
                    <label for="">in_price_package</label>
                    <input type="text" id="" class="form-control" name="in_price_package" placeholder="in_price_package" value=""/>
                </div>
                <div class="form-group">
                    <label for="">out_price_package</label>
                    <input type="text" id="" class="form-control" name="out_price_package" placeholder="out_price_package" value=""/>
                </div>
                <div class="form-group">
                    <label for="">make_date</label>
                    <input type="text" id="" class="form-control" name="make_date" placeholder="Position" value=""/>
                </div>
                <div class="form-group">
                    <label for="">expire_date</label>
                    <input type="text" id="" class="form-control" name="expire_date" placeholder="expire_date" value=""/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Submit </button>
                </div>
            </form>
    </div>
</div>