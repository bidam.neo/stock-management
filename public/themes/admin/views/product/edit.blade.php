<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Product Edit</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-5">
            <form action="/product/edit" class="form-horizontal" method="post">
                <input type="hidden" name="id" value="{{$product->id}}">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" id="" class="form-control" name="name" placeholder="Name" value="{{$product->name}}"/>
                </div>
                <div class="form-group">
                    <label for="">number</label>
                    <input type="text" id="" class="form-control" name="number" placeholder="number" value="{{$product->number}}"/>
                </div>
                <div class="form-group">
                    <label for="">country</label>
                    <input type="text" id="" class="form-control" name="country" placeholder="Company" value="{{$product->country}}"/>
                </div>
                <div class="form-group">
                    <label for="">code</label>
                    <input type="text" id="" class="form-control" name="code" placeholder="code" value="{{$product->code}}"/>
                </div>
                <div class="form-group">
                    <label for="">in_price</label>
                    <input type="text" required id="" class="form-control" name="in_price" placeholder="in_price" value="{{$product->in_price}}"/>
                </div>
                <div class="form-group">
                    <label for="">out_price</label>
                    <input type="text" id="" class="form-control" name="out_price" placeholder="out_price" value="{{$product->out_price}}"/>
                </div>
                <div class="form-group">
                    <label for="">in_price_package</label>
                    <input type="text" id="" class="form-control" name="in_price_package" placeholder="in_price_package" value="{{$product->in_price_package}}"/>
                </div>
                <div class="form-group">
                    <label for="">out_price_package</label>
                    <input type="text" id="" class="form-control" name="out_price_package" placeholder="out_price_package" value="{{$product->out_price_package}}"/>
                </div>
                <div class="form-group">
                    <label for="">make_date</label>
                    <input type="text" id="" class="form-control" name="make_date" placeholder="Position" value="{{$product->make_date}}"/>
                </div>
                <div class="form-group">
                    <label for="">expire_date</label>
                    <input type="text" id="" class="form-control" name="expire_date" placeholder="expire_date" value="{{$product->expire_date}}"/>
                </div>
                <div class="form-group">
                    <label for="">Category</label>
                    <select class="form-control" name="category" id="">

                        @foreach($categories as $cat)
                            <option value="{{$cat->id}}" <?php echo ($cat->id==$product->productType->id)? "selected":"" ?> >{{$cat->name}}</option>
                         @endforeach
                    </select>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-refresh"></i> Update </button>
                </div>
            </form>
    </div>
</div>