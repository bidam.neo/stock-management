<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Product List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="/product/new" class="btn btn-primary " style="float:right; margin-bottom: 20px;"> <i class="fa fa-plus"></i> Add More</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>number</th>
                <th>country</th>
                <th>code</th>
                <th>inprice</th>
                <th>outprice</th>
                <th>makedate</th>
                <th>expire date</th>
                <th>Category</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $pro)
                <tr>
                    <td>{{$pro->id}}</td>
                    <td>{{$pro->name}}</td>
                    <td>{{$pro->number}}</td>
                    <td>{{$pro->country}}</td>
                    <td>{{$pro->code}}</td>
                    <td>{{$pro->in_price}}</td>
                    <td>{{$pro->out_price}}</td>
                    <td>{{$pro->make_date}}</td>
                    <td>{{$pro->expire_date}}</td>
                    <td>{{$pro->productType->name}}</td>
                    <td width="10%">
                        <a href="/product/edit/{{$pro->id}}" class="btn btn-default"> <i class="fa fa-edit"></i> </a>
                        <a href="/product/delete/{{$pro->id}}" class="btn btn-danger delete"> <i class="fa fa-trash"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>