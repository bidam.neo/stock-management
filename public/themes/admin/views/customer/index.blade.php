<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Customer List</h1>


    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="/customer/new" class="btn btn-primary " style="float:right; margin-bottom: 20px;"> <i class="fa fa-plus"></i> Add More</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Company</th>
                <th>Address</th>
                <th>email</th>
                <th>position</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $cus)
                <tr>
                    <td>{{$cus->id}}</td>
                    <td>{{$cus->first_name}} {{$cus->last_name}}</td>
                    <td>{{$cus->company}}</td>
                    <td>{{$cus->add}}</td>
                    <td>{{$cus->email}}</td>
                    <td>{{$cus->position}}</td>
                    <td width="10%">
                        <a href="/customer/edit/{{$cus->id}}" class="btn btn-default"> <i class="fa fa-edit"></i> </a>
                        <a href="/customer/delete/{{$cus->id}}" class="btn btn-danger delete"> <i class="fa fa-trash"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>