<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">New customer</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-5">
            <form action="/customer/add" class="form-horizontal" method="post">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" id="" class="form-control" name="first_name" placeholder="First Name" value=""/>
                </div>
                <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" id="" class="form-control" name="last_name" placeholder="Last Name" value=""/>
                </div>
                <div class="form-group">
                    <label for="">Company</label>
                    <input type="text" id="" class="form-control" name="company" placeholder="Company" value=""/>
                </div>
                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" id="" class="form-control" name="add" placeholder="Address" value=""/>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" required id="" class="form-control" name="email" placeholder="Email" value=""/>
                </div>
                <div class="form-group">
                    <label for="">Position</label>
                    <input type="text" id="" class="form-control" name="position" placeholder="Position" value=""/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Submit </button>
                </div>
            </form>
    </div>
</div>