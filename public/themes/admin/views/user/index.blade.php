<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">User List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}} </td>
                    <td>
                        <a href="#edit" class="btn btn-default"> <i class="fa fa-edit"></i> Edit</a>
                        <a href="#delete" class="btn btn-danger delete"> <i class="fa fa-trash"></i> Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>