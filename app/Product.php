<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Get the post that owns the comment.
     */
    public function productType()
    {
        return $this->belongsTo('App\ProductType');
    }
}
