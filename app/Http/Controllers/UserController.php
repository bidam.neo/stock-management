<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->setupTheme();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'users' => User::all()
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('user.index', $view)->render();
    }
}
