<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct()
    {

        $this->setupTheme();
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'customers' => Customer::all()
        );

        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('customer.index', $view)->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id = null) {
        $customer = Customer::find($id);
        if (! $customer) {
            $customer = null;
        }
        $view = array(
            'customer' => $customer
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('customer.edit', $view)->render();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request) {
        $data = $request->all();
        $customer = Customer::find($data['id']);
        $customer->first_name = $data['first_name'];
        $customer->last_name = $data['last_name'];
        $customer->company = $data['company'];
        $customer->add = $data['add'];
        $customer->email = $data['email'];
        $customer->position = $data['position'];
        $customer->save();
        return redirect('/customer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request) {
        $data = $request->all();
        $customer = new Customer();
        $customer->first_name = $data['first_name'];
        $customer->last_name = $data['last_name'];
        $customer->company = $data['company'];
        $customer->add = $data['add'];
        $customer->email = $data['email'];
        $customer->position = $data['position'];
        $customer->save();
        return redirect('/customer');
    }


    public function getNew() {
        return $this->theme->scope('customer.new')->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id = null) {
        $customer = Customer::destroy($id);
        return redirect('/customer');
    }
}
