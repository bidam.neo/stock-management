<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->setupTheme();
        $this->middleware('auth');
    }

    public function index() {
        return redirect('/admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'name' => 'Teepluss'
        );

        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('admin.index', $view)->render();
    }
}
