<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->setupTheme();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'products' => Product::all()
        );

        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('product.index', $view)->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id = null) {
        $product = Product::find($id);
        if (! $product) {
            $product = null;
        }
        $view = array(
            'product' => $product,
            "categories" => ProductType::all()
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('product.edit', $view)->render();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request) {
        $data = $request->all();
        $product = product::find($data['id']);
        $product->name = $data['name'];
        $product->number = $data['number'];
        $product->country = $data['country'];
        $product->code = $data['code'];
        $product->in_price = $data['in_price'];
        $product->out_price = $data['out_price'];
        $product->in_price_package = $data['in_price_package'];
        $product->out_price_package = $data['out_price_package'];
        $product->make_date = $data['make_date'];
        $product->expire_date = $data['expire_date'];
        $product->product_type_id = $data['category'];
        $product->save();
        return redirect('/product');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request) {
        $data = $request->all();
//        dd($data);
        $product = new Product();
        $product->name = $data['name'];
        $product->number = $data['number'];
        $product->country = $data['country'];
        $product->code = $data['code'];
        $product->in_price = $data['in_price'];
        $product->out_price = $data['out_price'];
        $product->in_price_package = $data['in_price_package'];
        $product->out_price_package = $data['out_price_package'];
        $product->make_date = $data['make_date'];
        $product->expire_date = $data['expire_date'];
        $product->product_type_id = $data['category'];
        $product->save();
        return redirect('/product');
    }


    public function getNew() {
        $view = array(
            'categories' => ProductType::all()
        );
        return $this->theme->scope('product.new', $view)->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id = null) {
        Product::destroy($id);
        return redirect('/product');
    }
}
