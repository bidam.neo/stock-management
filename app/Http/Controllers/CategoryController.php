<?php

namespace App\Http\Controllers;

use App\ProductType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->setupTheme();
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'categories' => ProductType::all()
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('category.index', $view)->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id = null) {
        $category = ProductType::find($id);
        if (! $category) {
            $category = null;
        }
        $view = array(
            'category' => $category
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('category.edit', $view)->render();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request) {
        $data = $request->all();

        $productType = ProductType::find($data['id']);
        $productType->name = $data['name'];
        $productType->save();
        return redirect('/category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request) {
        $data = $request->all();

        $productType = new ProductType();
        $productType->name = $data['name'];
        $productType->save();
        return redirect('/category');
    }


    public function getNew() {
        return $this->theme->scope('category.new')->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id = null) {
        ProductType::destroy($id);
        return redirect('/category');
    }
}
