<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;

class SupplierController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->setupTheme();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'suppliers' => Supplier::all()
        );

        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('supplier.index', $view)->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id = null) {
        $supplier = supplier::find($id);
        if (! $supplier) {
            $supplier = null;
        }
        $view = array(
            'supplier' => $supplier
        );
        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('supplier.edit', $view)->render();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request) {
        $data = $request->all();
        $supplier = supplier::find($data['id']);
        $supplier->first_name = $data['first_name'];
        $supplier->last_name = $data['last_name'];
        $supplier->company = $data['company'];
        $supplier->add = $data['add'];
        $supplier->email = $data['email'];
        $supplier->tel = $data['tel'];
        $supplier->save();
        return redirect('/supplier');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postAdd(Request $request) {
        $data = $request->all();
        $supplier = new Supplier();
        $supplier->first_name = $data['first_name'];
        $supplier->last_name = $data['last_name'];
        $supplier->company = $data['company'];
        $supplier->add = $data['add'];
        $supplier->email = $data['email'];
        $supplier->tel = $data['tel'];
        $supplier->save();
        return redirect('/supplier');
    }


    public function getNew() {
        return $this->theme->scope('supplier.new')->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id = null) {
        Supplier::destroy($id);
        return redirect('/supplier');
    }

}
