<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{

    public function __construct()
    {

        $this->setupTheme();
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex() {
        $view = array(
            'invoices' => Invoice::all()
        );

        // home.index will look up the path 'public/themes/default/views/home/index.php'
        return $this->theme->scope('invoice.index', $view)->render();
    }
}
